<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends CheckAuthController
{   

    public function __construct(){
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('index')
            ->withPosts( Post::all()->reverse() )
            ->withUser( Auth::user() );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid = Auth::id();
        if ($request->img){
            $file = $request->img;
            $filename =  hash( 'sha256', uniqid() ) . '.' . $file->getClientOriginalExtension();;
            $file->storeAs('public/images', $filename);
            $path = "storage/images/" . $filename;
            $datas = [ 
                'label'   => $request->all()['label'] , 
                'text'      => $request->all()['text'], 
                'img'       => $path , 
                'user_id'   => $userid
                ];
            }
        else{
            $datas = [ 
                'label'   => $request->all()['label'] , 
                'text'      => $request->all()['text'], 
                'user_id'   => $userid
                ];
        }
        Post::create($datas);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
