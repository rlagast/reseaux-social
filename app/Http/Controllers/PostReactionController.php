<?php

namespace App\Http\Controllers;

use App\PostReaction;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class PostReactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
           

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Post $post, int $reaction_type)
    {
        $datas = [
            'user_id' => Auth::id(),
            'post_id' => $post->id,
            'type_id' => $reaction_type
        ];
        PostReaction::create($datas);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PostReaction  $postReaction
     * @return \Illuminate\Http\Response
     */
    public function show(PostReaction $postReaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PostReaction  $postReaction
     * @return \Illuminate\Http\Response
     */
    public function edit(PostReaction $postReaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PostReaction  $postReaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostReaction $postReaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PostReaction  $postReaction
     * @return \Illuminate\Http\Response
     */
    public function delete(Post $post, int $id)
    {
        PostReaction::where('user_id' , Auth::id())
                ->where('post_id', $post->id)
                ->where('type_id', $id)
                ->delete();
                return back();
    }
}
