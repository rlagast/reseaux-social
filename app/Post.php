<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $fillable = ['text', 'img', 'user_id', 'label'];

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function likes(){
        return $this->hasMany('App\PostReaction');
    }
}
