<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostReaction extends Model
{
    public $fillable = ['type_id', 'post_id', 'user_id'];
}
