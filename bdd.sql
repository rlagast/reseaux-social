-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.19 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour mercury
CREATE DATABASE IF NOT EXISTS `mercury` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mercury`;

-- Export de la structure de la table mercury. comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8mb4_unicode_ci,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_users` (`user_id`),
  KEY `comments_posts` (`post_id`),
  CONSTRAINT `comments_posts` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  CONSTRAINT `comments_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table mercury.comments : ~5 rows (environ)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`id`, `text`, `img`, `user_id`, `post_id`, `created_at`, `updated_at`) VALUES
	(1, 'oulala c\'est le very much premier commentaire', NULL, 1, 1, '2018-11-08 09:18:44', '2018-11-08 09:18:44'),
	(7, 'Je suis camille et je commente se post avec ma photo de profil par default', NULL, 2, 1, '2018-11-12 15:54:18', '2018-11-12 15:54:18'),
	(9, 'Franchement non', NULL, 4, 6, '2018-11-13 15:37:14', '2018-11-13 15:37:14'),
	(10, 'Wow', NULL, 4, 4, '2018-11-13 15:38:02', '2018-11-13 15:38:02'),
	(13, NULL, 'storage/images/9c7f254c3d8e37488abff19ad1278c175f43a1d3b54a43b40fd4915b59d99b19.jpg', 7, 6, '2018-11-13 16:00:28', '2018-11-13 16:00:28');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Export de la structure de la table mercury. comment_reactions
CREATE TABLE IF NOT EXISTS `comment_reactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `comment_id` int(10) unsigned NOT NULL,
  `type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comment_reactions_type` (`type_id`),
  KEY `comment_reactions_comments` (`comment_id`),
  KEY `comment_reactions_users` (`user_id`),
  CONSTRAINT `comment_reactions_comments` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`),
  CONSTRAINT `comment_reactions_type` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`),
  CONSTRAINT `comment_reactions_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table mercury.comment_reactions : ~0 rows (environ)
/*!40000 ALTER TABLE `comment_reactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment_reactions` ENABLE KEYS */;

-- Export de la structure de la table mercury. migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table mercury.migrations : ~7 rows (environ)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_11_07_152948_create_posts_table', 2),
	(4, '2018_11_07_153115_create_comments_table', 2),
	(5, '2018_11_07_153255_create_post_reactions_table', 2),
	(6, '2018_11_07_153543_create_comment_reactions_table', 2),
	(7, '2018_11_07_153624_create_type_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Export de la structure de la table mercury. password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table mercury.password_resets : ~0 rows (environ)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Export de la structure de la table mercury. posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_users` (`user_id`),
  CONSTRAINT `posts_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table mercury.posts : ~5 rows (environ)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`id`, `label`, `text`, `img`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 'Le Premier post', 'Oulala c\'est le premier post', '', 1, '2018-11-08 09:16:41', '2018-11-08 09:16:42'),
	(2, 'Le deuxieme post', 'Putain y\'a même un deuxième post c\'est incredble de jugador.', NULL, 1, '2018-11-08 14:56:34', '2018-11-08 14:56:35'),
	(4, 'Je m\'appel Camille et je fais mon premier Post', 'Ceci est mon premier post', 'storage/images/34ae2bfcd3e743355edf9ed180aba401be99df14667b74ef7b0f3894ab370771.jpg', 2, '2018-11-13 15:06:07', '2018-11-13 15:06:07'),
	(5, 'AAAAAAAAH CLIQUE!!!', 'S.E.V.R.A.N', NULL, 3, '2018-11-13 15:24:00', '2018-11-13 15:24:00'),
	(6, 'Je suis Bg ou pas?', NULL, 'storage/images/9c53aa07071eefaa2b1f5da42b4bd3e8039c92e2846d600fbde7d4c6b59db386.png', 3, '2018-11-13 15:25:39', '2018-11-13 15:25:39');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Export de la structure de la table mercury. post_reactions
CREATE TABLE IF NOT EXISTS `post_reactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_reactions_type` (`type_id`),
  KEY `post_reactions_post` (`post_id`),
  KEY `post_reactions_user` (`user_id`),
  CONSTRAINT `post_reactions_post` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  CONSTRAINT `post_reactions_type` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`),
  CONSTRAINT `post_reactions_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table mercury.post_reactions : ~10 rows (environ)
/*!40000 ALTER TABLE `post_reactions` DISABLE KEYS */;
INSERT INTO `post_reactions` (`id`, `user_id`, `post_id`, `type_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 2, 1, '2018-11-13 11:10:13', '2018-11-13 11:10:13'),
	(13, 1, 1, 1, '2018-11-13 13:47:15', '2018-11-13 13:47:15'),
	(15, 2, 2, 2, '2018-11-13 13:51:26', '2018-11-13 13:51:26'),
	(16, 2, 4, 1, '2018-11-13 15:06:27', '2018-11-13 15:06:27'),
	(17, 4, 5, 2, '2018-11-13 15:38:41', '2018-11-13 15:38:41'),
	(19, 4, 4, 1, '2018-11-13 15:38:50', '2018-11-13 15:38:50'),
	(22, 5, 4, 1, '2018-11-13 15:54:43', '2018-11-13 15:54:43'),
	(26, 6, 4, 1, '2018-11-13 15:57:40', '2018-11-13 15:57:40'),
	(27, 6, 2, 2, '2018-11-13 15:57:45', '2018-11-13 15:57:45'),
	(31, 8, 4, 2, '2019-01-08 14:42:26', '2019-01-08 14:42:26');
/*!40000 ALTER TABLE `post_reactions` ENABLE KEYS */;

-- Export de la structure de la table mercury. type
CREATE TABLE IF NOT EXISTS `type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table mercury.type : ~2 rows (environ)
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` (`id`, `label`, `created_at`, `updated_at`) VALUES
	(1, 'like', '2018-11-13 11:40:57', '2018-11-13 11:40:58'),
	(2, 'dislike', '2018-11-13 11:41:05', '2018-11-13 11:41:07');
/*!40000 ALTER TABLE `type` ENABLE KEYS */;

-- Export de la structure de la table mercury. users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profil_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'storage/images/default.jpg',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Export de données de la table mercury.users : ~8 rows (environ)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `profil_picture`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Remi Lg', 'rlagast@gmail.com', 'storage/images/photo.jpg', '2018-11-08 09:15:43', '$2y$10$Qlu3Zc8v.8mHfya5vJFMROzr7aX/5dWTo79TOCMg4oKZlAjguTcjK', 'K742P8qBVQ816ZApKdrOMd4VAMHxEIXvVHYS5GntHoFsUOYfsB1EmQp957Og', '2018-11-08 09:16:02', '2018-11-08 09:16:03'),
	(2, 'Camille', 'camille@gmail.com', 'storage/images/default.jpg', '2018-11-12 16:55:20', '$2y$10$2.tXAwvSB6tH/icpPg./1OdPzsjD19R7Mt8gE1SmxxufxMKcloIFy', 'k97UrqDiHF0VcxbnkXSfk6KnE8xz1ehuh8djFiqfPOkOHijH2HZyMJ46i9iE', '2018-11-12 15:53:07', '2018-11-12 15:53:07'),
	(3, 'Dozo9327', 'dozo9327@gmail.com', 'storage/images/7.jpg', '2018-11-13 16:36:48', '$2y$10$koTXeuoZ606KH6qEV50x/ec0sFyiqqdtW3B8JsHW9gAx4dWyh/eXO', 'LaCNXk46YE2ONC6EbczILDZJNVpqOlEAxE14CcFzHJl3BEYv06x4VBLTCd3X', '2018-11-13 15:19:53', '2018-11-13 15:19:53'),
	(4, 'Corinne', 'corinne@gmail.com', 'storage/images/9.jpg', '2018-11-13 16:36:49', '$2y$10$0X.asGv5FYMmyfr612AJ6.MF9Og0KW0mO8rFxNv06hNz87zXXc0QW', 'gDuCTB0um9Ul7qVMoLD5xF75lkjuoL5g7uiG5I2SHQ4ODB8bxDCUlF8zMHkc', '2018-11-13 15:36:31', '2018-11-13 15:36:31'),
	(5, 'PizzaNonBinaire', 'pizza@gmail.com', 'storage/images/8.jpg', '2018-11-13 17:00:01', '$2y$10$sRaWYqr/2MZoVyjnHxtR/Oy77xxRJsHzIneZqy3eUz5Lt44idXWsu', 'YABqIZuj7CHnt02es5OulrSs72oLcz1bLOAsOyedZsbAlL6YugEdBUldn1jE', '2018-11-13 15:39:41', '2018-11-13 15:39:41'),
	(6, 'KalashCri', 'kalash@gmail.com', 'storage/images/10.jpg', '2018-11-13 16:59:58', '$2y$10$IHL5Tf92.NyvLDjQ.9hqz.qXMuFIpY6NwcvqirEz8dnJCdPyQyYYC', 'i4ZYriIIffIxMKkxPhAZD1c4dhvEUHNGooORMye61TOwspKy1eCr28fn7d5p', '2018-11-13 15:56:11', '2018-11-13 15:56:11'),
	(7, 'B2o-LeDuc', 'leduc@gmail.com', 'storage/images/11.jpg', '2018-11-13 17:00:02', '$2y$10$yV.lqogf3rPGxvkz84AohOuq7gQm8AO63WDSohRMTR6qEzPZCf606', NULL, '2018-11-13 15:59:50', '2018-11-13 15:59:50'),
	(8, 'admin', 'admin@gmail.com', 'storage/images/default.jpg', NULL, '$2y$10$tDJfHAyvFSg8wC8/eEqsaOmFl8UAUVr7QGCNC0Iae6x4Wav9N3qiu', NULL, '2019-01-08 14:33:11', '2019-01-08 14:33:11');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
