@extends('layouts.app')

@section('content')
    <section id="posts">
    <form id="add-post" action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data">

        @csrf
        <h2>Title</h2>
        <input id='post-title' type="text" name="label">
        <h2>Content</h2>
        <textarea name="text"  cols="" rows=""></textarea><br>
        <label for="add-file">Choisir un fichier</label>
        <input id='add-file' class="post-file" type="file" name="img">
        <input type="submit" class="sub" value="Poster">


    </form>
    
    @foreach ($posts as $post)
    <div class="post">
        <div class="user">
            <img class="pp" src="{{ asset($post->user->profil_picture) }}" alt="">
            <span class="pseudo">{{$post->user->name}}</span>
        </div>
        <h3> {{ $post->label }}</h3>
        <p> {{ $post->text}} </p>
        <div class="media">
            @if ($post->img)
                <img src="{{$post->img}}" alt="">
            @endif
        </div>
        <div class="reactions">
           
            @if ( $user->hasReactTo($post, 1) > 0)
                <div>
                    <a href="{{ route('reaction.create', [$post, '1'] ) }}">
                        <img class="reaction" src="{{ asset( "storage/images/liked.svg" ) }}" alt="">
                    </a>
                    <span>{{ count($post->likes->where('type_id', 1)) }}</span>
                </div>
            @else
                <div>
                    <a href="{{ route('reaction.create', [$post, '1']) }}">
                        <img class="reaction" src="{{ asset( "storage/images/like.svg" ) }}" alt="">
                    </a>
                    <span>{{ count($post->likes->where('type_id', 1)) }}</span>
                </div>
            @endif

            
            @if ($user->hasReactTo($post, 2) > 0 )
                <div>
                    <a href="{{ route('reaction.delete', [$post, '2']) }}">
                        <img class="reaction" src="{{ asset( "storage/images/disliked.svg" ) }}" alt="">
                    </a>
                    <span>{{ count($post->likes->where('type_id', 2)) }}</span>
                </div>
            @else
                <div>
                    <a href="{{ route('reaction.create', [$post, '2']) }}">
                        <img class="reaction" src="{{ asset( "storage/images/dislike.svg" ) }}" alt="">
                    </a>
                    <span>{{ count($post->likes->where('type_id', 2)) }}</span>
                </div>
            @endif
            
        </div>
        <hr>
        <div class="comments">
        <form action="{{ route('comments.store') }}" method="post" enctype="multipart/form-data">

            @csrf
        
            <textarea name="text"  cols="" rows=""></textarea><br>
            <label for="post{{ $post->id }}">Choisir un fichier</label>
            <input id="post{{ $post->id }}" class="post-file" type="file" name="img">
            <input type="hidden" name="post_id" value="{{ $post->id }}">
            <input type="submit" class="sub" value="Poster">
        
        
        </form>
        
        @foreach ($post->comments as $comment)
        <div class="user">
            <img class="pp pp-com" src="{{ asset($comment->user->profil_picture) }}" alt="">
            <span class="pseudo pseudo-com">{{$comment->user->name}}</span>
        </div>
            <p>{{ $comment->text }}</p>
            <div class="media">
            @if ($comment->img)
                <img src="{{ asset($comment->img) }}" alt="">
            @endif
            </div>
            <hr>
        @endforeach
        </div>
    </div>
    @endforeach
</section>
@endsection