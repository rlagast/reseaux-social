<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'PostController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('comments', 'CommentController');

Route::resource('posts', 'PostController');

Route::get('posts/{post}/reactions/{reaction}', 'PostReactionController@store')
        ->where('post',  '[0-9]*')
        ->where('reaction',  '[0-9]*')
        ->name('reaction.create');

Route::get('posts/{post}/reactions/delete/{reaction}', 'PostReactionController@delete')
        ->where('post',  '[0-9]*')
        ->where('reaction',  '[0-9]*')
        ->name('reaction.delete');